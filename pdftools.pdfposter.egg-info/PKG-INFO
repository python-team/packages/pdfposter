Metadata-Version: 2.1
Name: pdftools.pdfposter
Version: 0.8.1
Summary: Scale and tile PDF images/pages to print on multiple pages.
Home-page: https://pdfposter.readthedocs.io/
Author: Hartmut Goebel
Author-email: h.goebel@crazy-compilers.com
License: GNU General Public License v3 or later (GPLv3+)
Download-URL: https://pypi.org/project/pdftools.pdfposter/
Description: ==========================
        pdfposter
        ==========================
        
        -------------------------------------------------------------
        Scale and tile PDF images/pages to print on multiple pages.
        -------------------------------------------------------------
        
        :Author:    Hartmut Goebel <h.goebel@crazy-compilers.com>
        :Version:   Version 0.8.1
        :Copyright: 2008-2022 by Hartmut Goebel
        :License:   GNU Public License v3 or later (GPL-3.0-or-later)
        :Homepage:  https://pdfposter.readthedocs.io/
        
        ``Pdfposter`` can be used to create a large poster by building it from
        multiple pages and/or printing it on large media. It expects as input a
        PDF file, normally printing on a single page. The output is again a
        PDF file, maybe containing multiple pages together building the
        poster.
        The input page will be scaled to obtain the desired size.
        
        This is much like ``poster`` does for Postscript files, but working
        with PDF. Since sometimes poster does not like your files converted
        from PDF. :-) Indeed ``pdfposter`` was inspired by ``poster``.
        
        For more information please refer to the manpage or visit
        the `project homepage <https://pdfposter.readthedocs.io/>`_.
        
        
        Requirements and Installation
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        
        ``Pdfposter`` requires
        
        * `Python`__  (3.6—3.10, but newer versions should work, too),
        * `setuptools`__ or `pip`__ for installation, and
        * `PyPDF2`__ (2.1.1 or newer).
        
        __ https://www.python.org/download/
        __ https://pypi.org/project/setuptools
        __ https://pypi.org/project/pip
        __ http://mstamy2.github.io/PyPDF2/
        
        
        .. Emacs config:
         Local Variables:
         mode: rst
         End:
        
Keywords: pdf,poster
Platform: UNKNOWN
Classifier: Development Status :: 5 - Production/Stable
Classifier: Environment :: Console
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: End Users/Desktop
Classifier: Intended Audience :: System Administrators
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Natural Language :: English
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.6
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Topic :: Printing
Classifier: Topic :: Utilities
Description-Content-Type: text/x-rst
